<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DEP-10: parallelized release management</title>

<link rel="stylesheet" href="../../style.css" type="text/css" />
<link rel="stylesheet" href="../../local.css" type="text/css" />


</head>
<body>

<div class="header">
<span>

</div>


<div class="actions">
<ul>


<li><a href="../../recentchanges/">RecentChanges</a></li>


<li><a href="http://svn.debian.org/viewvc/dep/web/deps/dep10.mdwn">History</a></li>



</ul>
</div>




<div id="content">
<pre><code>Title: parallelized release management"
DEP: 10
State: DRAFT
Date: 2011-04-30
Drivers: Sean Finney &lt;seanius@debian.org&gt;,
 Raphaël Hertzog &lt;hertzog@debian.org&gt;
URL: http://dep.debian.net/deps/dep10
Source: http://anonscm.debian.org/viewvc/dep/web/deps/dep10.mdwn
License: GPL
Abstract: Proposal for changes to release management methodology and 
 infrastructure, allowing the Debian release process to function
 in parallel to non-release related updates.
</code></pre>

<div class="toc">
<ol>
	<li class="L1"><a href="#index1h1">Introduction / Problem scope</a>
	</li>
	<li class="L1"><a href="#index2h1">Past and present release methods</a>
	<ol>
		<li class="L2"><a href="#index1h2">Frozen (< 2000)</a>
		</li>
		<li class="L2"><a href="#index2h2">Testing (2000-Present)</a>
		</li>
	</ol>
	</li>
	<li class="L1"><a href="#index3h1">Constraints and requirements for any change to the release process</a>
	</li>
	<li class="L1"><a href="#index4h1">A host of proposals</a>
	<ol>
		<li class="L2"><a href="#index3h2">Branch testing at freeze time (a.k.a frozen v2.0)</a>
		</li>
		<li class="L2"><a href="#index4h2">Implement an unstable-updates</a>
		</li>
		<li class="L2"><a href="#index5h2">Implement "Debian PPA's" and advocate their use</a>
		</li>
		<li class="L2"><a href="#index6h2">Implement a rolling release</a>
		</li>
	</ol>
	</li>
</ol>
</div>


<h1><a name="index1h1"></a>Introduction / Problem scope</h1>

<p>Currently, as the project nears a new stable release, a freeze is instituted
on the testing suite.  The freeze is put in place to allow the release team
to focus on resolving the remaining Release Critical (RC) bugs for the next
stable release, and at the same time to prevent regressions from new uploads.
Typically the freeze begins as an advisory "soft freeze", which over
time increases in strictness and levels of enforcement.</p>

<p>Unsuprisingly, as the strictness of the freeze increases, there is an
inversely proportional decrease in other non-release targeted maintainer
activity.  Since unstable still is the preferred route for packages to
reach the new release during this period, maintainers are highly discouraged
and in some cases prevented from doing non-release targeted activities
in unstable.</p>

<p><a name="introduction-probs">
The reduction of such non-release activity is viewed as problematic in
this DEP, for some inter-related reasons:</p>

<ul>
<li>New features and innovations are put on hold, or at least not commonly
available, until after the release is made.</li>
<li>Overall Maintainer activity decreases as freezes persist.</li>
<li>Potential userbase is lost (missing feature X, switch distro).</li>
<li>The volatility of testing/unstable increases (and thus quality decreases)
with a deluge of new uploads after the freeze is lifted.</li>
</ul>


<p>As Debian is well known for taking a "release when it's ready" approach, the
freeze periods are generally known to last considerable amounts of time.
Consider the last three freezes:</p>

<ul>
<li>etch: 4 months</li>
<li>lenny: 7 months</li>
<li>squeeze: 6 months</li>
</ul>


<p>This means, in rough terms, that the when testing thaws, that the Debian
project may be starting from a state half a year behind comparable
distributions; the project is, in essence, starting from a 6 month
standstill.</p>

<p>This standstill, as well as the subsequent rush of "post-thaw" uploads,
will introduce further delays to the next release, as release goals
will be set relative to this handicapped starting point and a non-trivial
amount of developer effort will be spent reconciling problems in the ensuing
rush of uploads and transitions.</p>

<h1><a name="index2h1"></a>Past and present release methods</h1>

<h2><a name="index1h2"></a>Frozen (< 2000)</h2>

<p>Before the introduction of testing, Debian used a simple release process
where the unstable suite was snapshotted into a <code>frozen</code> suite.  This suite
would then be used exclusively for preparing the next stable release, with
unstable continuing in parallel.</p>

<pre><code>Before freeze       Freeze           Release

[unstable/sid]--------------------------------------------------------------
                    \
                    [frozen].-.-.-.-.[stable/R_N].-.-.-.-.-.-.-.-.-.-.-.-.-.

[stable/R_N-1].-.-.-.-.-.-.-.-.-.-.-.[oldstable/R_N-1].-.-.-(EOL)

--------: Normal activity.  Standard rules for uploads and migrations.
.-.-.-.-: Release targeted activity.  Freezes and limited uploads. 
\ \ \ \ : Package migration activity.
</code></pre>

<h3><a name="index1h3"></a>Use cases with <code>frozen</code></h3>

<p>Users were divided into two sets, those using <code>unstable</code> and those using
<code>stable</code>.  Very few users would use both, as the two lines of development
would quickly diverge from each other.</p>

<h3><a name="index2h3"></a>Benefits with <code>frozen</code></h3>

<ul>
<li>Work in unstable continued, unaffected by the freeze.</li>
</ul>


<h3><a name="index3h3"></a>Problems with <code>frozen</code></h3>

<ul>
<li>Release work started from a buggy suite.</li>
<li>Duplicated effort in unstable/frozen.</li>
<li>RM had more work/responsibilities, and was prone to burn-out.</li>
</ul>


<h2><a name="index2h2"></a>Testing (2000-Present)</h2>

<p>The testing suite was introduced in Debian between the release of potato
and woody, in the fall of 2000<a href="http://lists.debian.org/debian-devel/2000/08/msg00906.html">1</a>.  The goal was to provide
a suite that was in a better state for release preparation, by having
both automated and manual tools to keep down the level of bugs and
general volatility.</p>

<p>As a pleasant and convenient side-effect, the new suite also provided
a "slightly less buggy unstable" for developers and end-users, who wanted
newer software/features not available in stable, but wanted some level
of protection to the relatively unpredictable nature of unstable.</p>

<pre><code>Before release         Freeze              Release

[unstable/sid]----------.--.--.--.-.-.-.-.----------------------------------
     \ \ \ \ \ \ \ \ \  \   \    \       \   \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \
[testing/R_N]----------.-.-.-.-.-.-.-.-.-.-[testing/R_N+1]------------------
                                      / / \
                                     / /   [stable/R_N].-.-.-.-.-.-.-.-.-.-.
                                    / /          /          /           /
                               [R_N p-u].-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-

[stable/R_N-1].-.-.-.-.-.-.-.-.-.-.-.-.-.-[oldstable/R_N-1].-.-.-(EOL)

--------: Normal activity.  Standard rules for uploads and migrations.
.-.-.-.-: Release targeted activity.  Freezes and limited uploads. 
\ \ \ \ : Package migration activity.  Spacing of marks is a rough
          indication of frequency.
</code></pre>

<p>During the freeze, the testing suite becomes entirely dedicated to the
release work.  In practice, this also means that unstable is also to
some extent frozen from non-release activity for many packages, since
it still serves as the main route to testing for new uploads.</p>

<h3><a name="index4h3"></a>Use cases with <code>testing</code></h3>

<p>The introduction of the new suite also introduced a type of continuum
in which users now have more flexibility in selecting what to have installed.</p>

<ul>
<li><code>unstable</code>: only the "bleeding edge" of Debian updates.</li>
<li><code>testing</code>: only the "leading edge" of Debian updates.</li>
<li><code>testing/unstable</code>: A hybrid solution of the two previous use cases,
 allowing for a reasonable balance between them.</li>
<li><code>stabe</code>: The latest stable release.</li>
<li><code>stable/testing</code>: the stable release with perhaps some newer packages
 installed, typically also making use of "APT pins" to prevent unwanted
 upgraes.  Less common now with the inclusion of backports and volatile
 as official Debian services.</li>
</ul>


<h3><a name="index5h3"></a>Benefits with <code>testing</code></h3>

<ul>
<li>Release branch has far fewer bugs at the start of the release process.</li>
<li>(Ideally) shorter release process with fewer problems.</li>
<li>Large user-base for testing stable-targeted fixes.</li>
</ul>


<h3><a name="index6h3"></a>Problems with <code>testing</code></h3>

<p>(See <a href="#introduction-probs">Introduction</a>)</p>

<h1><a name="index3h1"></a>Constraints and requirements for any change to the release process</h1>

<p>Based on feedback on the <code>debian-devel</code> mailing list, this proposal
makes a number of assumptions about constraints and requirements for
any alteration of the current release process.</p>

<ol>
<li>Any irreconcilable conflict should side towards release preparation</li>
<li>activity which can not be done in parellel must only be done for
release preparation.</li>
<li>any proposal must be no less safe than the current system with
regards to human error (uploads to wrong suite, etc, un-unannounced
transitions, etc).</li>
<li>Debian stable releases need sufficient test coverage by users</li>
<li>a testing (or testing-like) quarantine is needed for QA purposes</li>
<li>sufficient users should be using this quarantine to give good coverage.</li>
<li>Any parallel work should interfere minimally with release preparations</li>
<li>newer software should not prevent an upload path to the release.</li>
<li>library transitions and similar should not cause unreasonable overhead.</li>
<li>it must still be possible to remove packages from the release.</li>
<li>The upgrade path and archive state must be consistant and reconcilable</li>
<li>packages updated outside of the release should always have higher versions
than in the release, even if both were updated.</li>
<li>If the suite contents are to change post-release, the proposal must
account for renamed/duplicate/removed packagets.</li>
</ol>


<h1><a name="index4h1"></a>A host of proposals</h1>

<p>During the post-squeeze release feedback on debian-devel, there has been a
number of ideas and suggestions for how the freeze of the release process
could be minimized, circumvented, or avoided altogether.  Some of the more
popular and/or controversial alternatives will be discussed and analyzed
in the following section.</p>

<h2><a name="index3h2"></a>Branch <code>testing</code> at freeze time (a.k.a <code>frozen v2.0</code>)</h2>

<p>At some point during release preparation, somewhere between the "soft" and
"hard" freeze points of the current release, the <code>testing</code> suite is split
into two new independant suites.  The first of these suites is used for
continuing the (frozen) next stable release, while the other continues
receiving updates via <code>unstable</code>.</p>

<pre><code>Before release         Freeze                 Release

[unstable/sid]--------------------------------------------------------------
          \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \
[testing/R_N]----------[testing/R_N+1]--------------------------------------
                           \ \ \  \   \    \
                            [R_N].-.-.-.-.-.-.[stable/R_N].-.-.-.-.-.-.-.-.-
                             /    /   /  / / / /         /                /
                       [R_N p-u].-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-

[stable/R_N-1].-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-[oldstable/R_N-1]-.-(EOL)

--------: Normal activity.  Standard rules for uploads and migrations.
.-.-.-.-: Release targeted activity.  Freezes and limited uploads. 
\ \ \ \ : Package migration activity.  Spacing of marks is a rough
          indication of frequency.
</code></pre>

<p>This is very similar to the previous <code>frozen</code> release process, though
with the key difference that at the point the branch-off occurs, the
suite is in a far better condition due to the additional and continuous
QA recieved in the <code>unstable</code>-><code>testing</code> process.</p>

<p>As a consequence, work in <code>unstable</code> will quickly diverge from the state
of the frozen release, which has to immediate consequences:
 * The release team will rely on using the corresponding <code>-proposed-updates</code>
   pseudo-suite for release-targeted uploads, as undesired transitions/updates
   in <code>unstable</code> will quickly pare off the candidates for direct migration.
 * The default path (and userbase) of <code>unstable</code>-><code>testing</code> can no longer
   be relied upon for the same amount of QA in the release preparation
   process.</p>

<h3><a name="index7h3"></a>Benefits</h3>

<h3><a name="index8h3"></a>Problems</h3>

<h3><a name="index9h3"></a>Conclusion</h3>

<h2><a name="index4h2"></a>Implement an <code>unstable-updates</code></h2>

<p>A new pseudo-suite <code>unstable-updates</code> (or some other creative name left
for a later exercise) is established, which functions in a manner similar
to the existing <code>experimental</code> pseudo-suite.  A key difference from
<code>experimental</code> is that this suite is explicitly expected to feed packages
to the <code>unstable</code> suite, and thus the contents should be subject to the
same quality standards expected of <code>unstable</code> uploads.  Outside of the
release process, this feeding can be accomplished either by automatic
<code>britney</code> migrations or some form of semi-automatic "gating" process
requiring ACK's from either the maintainer or the release team.</p>

<p>During freeze, which continues as it does today, the release team disables
the migrations from <code>unstable-updates</code> to <code>unstable</code>.  The former, in
effect, becomes a staging area for updates to the <em>following</em> release.
Post-release, the migration is re-activated, possibly with some additional
controls allowing the updates to occur in a more orderly fashion if needed.</p>

<pre><code>Before release         Freeze              Release

[unstable-updates]----------------------------------------------------------
     \ \ \ \ \ \ \ \ \      \            \   \   \ \ \ \ \ \ \ \ \ \ \ \ \ \
[unstable/sid]----------.--.--.--.-.-.-.-.----------------------------------
     \ \ \ \ \ \ \ \ \  \   \    \       \   \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \
[testing/R_N]----------.-.-.-.-.-.-.-.-.-.-[testing/R_N+1]------------------
                                      / / \
                                     / /   [stable/R_N].-.-.-.-.-.-.-.-.-.-.
                                    / /          /          /           /
                               [R_N p-u].-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-

[stable/R_N-1].-.-.-.-.-.-.-.-.-.-.-.-.-.-[oldstable/R_N-1].-.-.-(EOL)

--------: Normal activity.  Standard rules for uploads and migrations.
.-.-.-.-: Release targeted activity.  Freezes and limited uploads. 
\ \ \ \ : Package migration activity.  Spacing of marks is a rough
          indication of frequency.
</code></pre>

<h3><a name="index10h3"></a>Benefits</h3>

<h3><a name="index11h3"></a>Problems</h3>

<h3><a name="index12h3"></a>Conclusion</h3>

<h2><a name="index5h2"></a>Implement "Debian PPA's" and advocate their use</h2>

<p>Another alternative which has been brought up at several points in
discussion is the use of <em>Personal Package Archives</em>, a.k.a. <em>PPA</em>'s,
a.k.a. <em>DSR</em>'s, as a means to provide packages updates entirely outside
of the release process.  Individual maintainers, or teams of related
projects, could establish and host independant and self-contained
repositories for newer packages.</p>

<p>During a release freeze, these repositories are created on an ad-hoc
basis to host any updated packages not targeted for release.  Users seeking
newer software would be directed to add additional APT sources to their
package manager configuration to enable the updates.</p>

<p>Additionally, PPA's could serve several other purposes apart from avoiding
the testing freeze.  For example, Outside of the release proucess, they
can continue to host newer packages than are immediately available in
unstable, in a manner similar to the existing <code>experimental</code> pseudo-suite.</p>

<p>In a manner similar to the <code>backports.org</code> services, PPA's could be
established to serve self-contained sets of backported packages to
previous stable releases.  For example, newer versions of popular
desktop software, or entirely desktop environments, could be hosted
in these self-contained release areas.</p>

<p>PPA's could also be used by maintainers and/or the release team for
preparation of package transitions outside of <code>unstable</code>, providing an
orderly way to update sets of interdependant packages and reducing
overall breakage.  This would lead to not only a higher quality experience
in <code>testing/unstable</code>, but also streamline the process for subsequent
releases, which would experience fewer blockages in work due to
ongoing transitions.</p>

<h3><a name="index13h3"></a>Benefits</h3>

<h3><a name="index14h3"></a>Problems</h3>

<h3><a name="index15h3"></a>Conclusion</h3>

<h2><a name="index6h2"></a>Implement a <code>rolling</code> release</h2>

<p><strong>NOTE</strong>: this section needs to be re-worked.  We're not talking about
implementing rolling, though there are some key points which do need to
be discussed in how this implementation would affect and work with rolling.
The idea of a Debian <code>rolling</code> release has been discussed with increased
amounts of interest and seriousness during and after the <code>squeeze</code> release
cycle.</p>

<p>While it has been proposed in several shapes and forms, the overarching
concept is that users should be provided a constantly updating release
similar to the current <code>testing</code> suite, with some key differences:</p>

<ul>
<li>ability to avoid/circumvent blockage from ongoing transitions in
<code>unstable</code>.</li>
<li>a more official level of support (security, RC bug fixes, etc)</li>
<li>continuous updates, even during release freezes</li>
</ul>


<p>It's worth note that the motivation/proposal for <code>rolling</code> doesn't
entirely overlap with the motivations behind the DEP.  Certainly both
proposals involve the ability to have continuous updates during the
entire release cycle, but beyond that the additional desires regarding
rolling are orthogonal (or only related in as much as they might place
constraints on the complementing implementations)</p>

<p>Furthermore, as stated, there have been several <em>different</em>
visions/implementations of <code>rolling</code> discussed.</p>

<h3><a name="index16h3"></a><code>rolling</code> by way of "alternate entry points"</h3>

<p>A new and entirely independant <code>rolling</code> suite is established, in parallel
to testing.  Both suites have a default entry point of the <code>unstable</code> suite,
which feeds both suites through a similar manner.</p>

<p>While packages may migrate into rolling via typical <code>unstable</code>
uploads, there also exist means to directly upload to the suite via a
<code>rolling-proposed-updates</code> or similarly named pseudo-suite.  This
pseudo suite would be an overlay on top of the standard <code>rolling</code> suite,
allowing for the testing and acceptance of direct fixes without the
risk of being blocked by any ongoing transitions or otherwise unsatisfiable
dependency chains.</p>

<p>During a release freeze,
as the default entry point for <code>rolling</code>,</p>

<p><code>rolling-proposed-updates</code> could take the
additional role of accepting new non-release targeted uploads, or
an additional entry point could be defined to replace the role of
unstable.</p>

<h3><a name="index17h3"></a><code>rolling</code> by way of "override hints"</h3>

<h3><a name="index18h3"></a>Benefits</h3>

<h3><a name="index19h3"></a>Problems</h3>

<h3><a name="index20h3"></a>Conclusion</h3>

</div>

<div id="footer">
<div id="pageinfo">







<div class="pagedate">
Last edited <span class="date">Thu, 26 Dec 2013 11:13:33 +0000</span>
</div>

</div>

<!-- from Debian Enhancement Proposals -->
</div>

</body>
</html>
