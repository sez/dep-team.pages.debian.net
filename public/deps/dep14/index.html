<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DEP-14: Recommended layout for Git packaging repositories</title>

<link rel="stylesheet" href="../../style.css" type="text/css" />
<link rel="stylesheet" href="../../local.css" type="text/css" />


</head>
<body>

<div class="header">
<span>

</div>


<div class="actions">
<ul>


<li><a href="../../recentchanges/">RecentChanges</a></li>


<li><a href="http://svn.debian.org/viewvc/dep/web/deps/dep14.mdwn">History</a></li>



</ul>
</div>




<div id="content">
<pre><code>Title: Recommended layout for Git packaging repositories
DEP: 14
State: DRAFT
Date: 2016-11-09
Drivers: Raphael Hertzog &lt;hertzog@debian.org&gt;
URL: http://dep.debian.net/deps/dep14
Source: http://anonscm.debian.org/viewvc/dep/web/deps/dep14.mdwn
Abstract:
 Recommended naming conventions in Git repositories used
 to maintain Debian packages
</code></pre>

<h1>Introduction</h1>

<p>This is a proposal to harmonize the layout of Git repositories
used to maintain Debian packages. The goals are multiple:</p>

<ul>
<li><p>making it easier for Debian and its derivatives to build upon
their respective Git repositories (with the possibility
to share a common one in some cases)</p></li>
<li><p>make it easier to switch between various git packaging helper
tools. Even if all the tools don't implement the same worflow, they
could at least use the same naming conventions for the same things
(Debian/upstream release tags, default packaging branch, etc.).</p></li>
</ul>


<h1>Scope</h1>

<p>This proposal defines naming conventions for various Git branches
and Git tags that can be useful when doing Debian packaging work.
The hope is that maintainers of git packaging helper tools will adopt
those naming conventions (in the default configuration of their tools).</p>

<h1>Generic principles</h1>

<h2>Vendor namespaces</h2>

<p>Each "vendor" uses its own namespace for its packaging related
Git branches and tags: <code>debian/*</code> for Debian, <code>ubuntu/*</code> for Ubuntu, and
so on.</p>

<p>Helper tools should usually rely on the output of <code>dpkg-vendor --query vendor</code>
to find out the name of the current vendor. The retrieved string must be
converted to lower case. This allows users to override the current vendor
by setting <code>DEB_VENDOR</code> in their environment (provided that the vendor
information does exist in <code>/etc/dpkg/origins/</code>).</p>

<p>If <code>dpkg-vendor</code> is not available, then they should assume "debian" is the
current vendor. Helper tools can also offer a command-line option to
override the current vendor if they desire.</p>

<h2>Version mangling</h2>

<p>When a Git tag needs to refer to a specific version of a Debian package,
the Debian version needs to be mangled to cope with Git's restrictions.
This mangling is deterministic and reversible:</p>

<ul>
<li>Each colon (<code>:</code>) is replaced with a percent (<code>%</code>)</li>
<li>Each tilde (<code>~</code>) is replaced with an underscore (<code>_</code>)</li>
<li>A hash (<code>#</code>) is inserted between each pair of adjacent dots (<code>..</code>)</li>
<li>A hash (<code>#</code>) is appended if the last character is a dot (<code>.</code>)</li>
<li>If the version ends in precisely <code>.lock</code>
(dot <code>l</code> <code>o</code> <code>c</code> <code>k</code>, lowercase, at the end of the version),
a hash (<code>#</code>) is inserted after the dot, giving <code>.#lock</code>.</li>
</ul>


<p>This can be expressed concisely in the following Perl5 statements:</p>

<pre><code> y/:~/%_/;
 s/\.(?=\.|$|lock$)/.#/g;
</code></pre>

<p>The reverse transformation is:</p>

<ul>
<li>Each percent (<code>%</code>) is replaced with a colon (<code>:</code>)</li>
<li>Each underscore (<code>_</code>) is replaced with a tilde (<code>~</code>)</li>
<li>Each hash (<code>#</code>) is deleted</li>
</ul>


<h1>Packaging branches and tags</h1>

<p>In general, packaging branches should be named according to the codename
of the target distribution. We differentiate however the case of development
releases from other releases.</p>

<h2>For development releases</h2>

<p>Packages uploaded to the current development release should be prepared
in a <code>&lt;vendor&gt;/master</code> branch.</p>

<p>However, when multiple development releases are regularly used (for
example <code>unstable</code> and <code>experimental</code> in Debian), it is also accepted to
name the branches according to the codename or the suite name of the
target distribution (aka <code>debian/sid</code> or <code>debian/unstable</code>, and
<code>debian/experimental</code>). Those branches can be short-lived (i.e. they exist
only until they are merged into <code>&lt;vendor&gt;/master</code> or until the version in
the associated repository is replaced by the version in <code>&lt;vendor&gt;/master</code>)
or they can be permanent (in which case <code>&lt;vendor&gt;/master</code> should not
exist).</p>

<p>NOTE: If the Git repository listed in debian/control's <code>Vcs-Git</code> field does
not indicate an explicit branch (with the <code>-b &lt;branch&gt;</code> suffix) then it should
have its HEAD point to the branch where new upstream versions are being
packaged (that is one of the branches associated to a development release).
The helper tools that do create those repositories should use a command
like <code>git symbolic-ref HEAD refs/heads/debian/master</code> to update HEAD
to point to the desired branch.</p>

<h2>For stable releases</h2>

<p>When a package targets any release that is not one of the usual
development releases (i.e. stable releases or a frozen development
release), it should be prepared in a branch named with the codename of the
target distribution. In the case of Debian, that means for example
<code>debian/jessie</code>, <code>debian/wheezy</code>, <code>debian/wheezy-backports</code>, etc.  We
specifically avoid "suite" names because those tend to evolve over time
("stable" becomes "oldstable" and so on).</p>

<p>Security updates and stable updates are expected to be handled on
the branch of the associated distribution. For example, in the case of
Debian, uploads to <code>wheezy-security</code> or <code>wheezy-proposed-updates</code>
are prepared on the <code>debian/wheezy</code> branch. Shall there be a need to
manage different versions of the packages in both repositories, then
the branches <code>debian/wheezy-security</code> and <code>debian/wheezy-updates</code>
can be used.</p>

<h2>Tagging package releases</h2>

<p>When releasing a Debian package, the packager should create and push
a (preferably signed) tag named <code>&lt;vendor&gt;/&lt;version&gt;</code>. For example, a
Debian maintainer releasing a package with version 2:1.2~rc1-1 would
create a tag named <code>debian/2%1.2_rc1-1</code> whereas an Ubuntu packager
releasing a package with version 1.3-0ubuntu1 would use
<code>ubuntu/1.3-0ubuntu1</code>. The tags should point to the exact commit that was
used to build the corresponding upload.</p>

<h1>Managing upstream sources</h1>

<h2>Importing upstream release tarballs in Git</h2>

<p>If the Git workflow in use imports the upstream sources from released
tarballs, this should be done under the "upstream" namespace. By default,
the latest upstream version should be imported in the <code>upstream/latest</code>
branch and when packages for multiple upstream versions are maintained
concurrently, one should create as many upstream branches as required.</p>

<p>Their name should be based on the major upstream version tracked:
for example when upstream maintains a stable 1.2 branch and releases
1.2.x minor releases in that branch, those releases should be imported
in a <code>upstream/1.2.x</code> branch (the ".x" suffix makes it clear that we are
referring to a branch and not to the tag corresponding the upsteam 1.2
release). If the upstream developers use codenames to refer to their
releases, the upstream branches can be named according to those codenames.</p>

<p>Helper tools should detect when the upstream version imported is lower
than the latest version available in <code>upstream/latest</code> and should offer
either to create a new branch (based on the highest version available in
the history that is still smaller than the imported version) or to pick
another pre-existing upstream branch.</p>

<h2>Importing upstream releases from Git tags</h2>

<p>When the packaging branches are directly based on the upstream Git
branches, upstream usually also provide proper Git tags that can be reused
for official releases.</p>

<p>If helper tools have to identify the upstream commit corresponding to a
given upstream release, they should be able to find it out by looking up
the following Git tags: <code>upstream/&lt;version&gt;</code>, <code>&lt;version&gt;</code> and
<code>v&lt;version&gt;</code>.</p>

<p>The <code>upstream/&lt;version&gt;</code> tag would be created by the package maintainer
when needed: for example when it does a release based on a Git snapshot or
when the tag naming scheme used by upstream is not following the above
rules.</p>

<h2>About pristine-tar</h2>

<p>If the package maintainers use the pristine-tar tool to efficiently store
a byte-for-byte copy of the upstream tarballs, this should be done in the
<code>pristine-tar</code> branch.</p>

<h1>Native packages</h1>

<p>The above conventions mainly cater to the case where the upstream
developers and the package maintainers are not the same set of persons.</p>

<p>When upstream is Debian (or one of its derivative), the upstream vendor
should not use the usual <code>&lt;vendor&gt;/</code> prefix (but all others vendors should
do so). The main development branch can be named <code>master</code> instead of
the codename of the target distribution (although you are free to still
use the codename if you wish so).</p>

<p>When the package is shipped as a native source package (i.e. a single
tarball with no differentiation of the upstream sources and of the
packaging files), the concept of "upstream sources" is irrelevant and the
associated <code>upstream/*</code> branches are irrelevant too. Note that even if the
upstream vendor ships the package as a native package, the downstream
vendors can still can opt to package it in a non-native way.</p>

<h1>Patch queue tags</h1>

<p>A patch queue branch is a (possibly temporary) branch used to define the
set of upstream changes that the package will contain, its content is
generally used to later update <code>debian/patches</code> in the resulting
source package.</p>

<p>Helper tools that want to store a copy of the "patch queues branches" for
released versions of the packages should create a tag named
<code>&lt;vendor&gt;/patches/&lt;version&gt;</code>. That tag should usually point to the
upstream branch with vendor patches applied on top of it.</p>

<h1>Other recommendations</h1>

<p>This sections ventures a bit outside of the topic of naming conventions
with some further suggestions for package maintainers and developers
of helper tools.</p>

<h2>What to store in the packaging branches</h2>

<p>It is recommended that the packaging branches contain both the upstream
sources and the Debian packaging. Users who have cloned the repository
should be able to run <code>dpkg-buildpackage -b -us -uc</code> without doing
anything else (assuming they have the required build dependencies).</p>

<p>It is also important so that contributors are able to use the tool of their
choice to update the debian/patches quilt series: multiple helper tools
need the upstream sources in Git to manage this patch series as a Git
branch.</p>

<p>When you have good reasons to only store the <code>debian</code> packaging directory
(for example when the uptream sources are really huge and contains mostly
non-patchable data), you can do so but you should then document
this in <code>debian/README.source</code> along with some explanations of the tool
to use to build the package.</p>

<h2>About repacked upstream sources</h2>

<p>When the upstream sources needs to be repacked (for example to drop some
non-DFSG compliant files), then the branches and tags under the
<code>upstream/</code> prefix should actually contain the repacked sources.</p>

<p>How the problematic files are pruned does not matter. What matters is that
what is stored in the <code>upstream/*</code> namespace are the sources that package
maintainers are effectively using.</p>

<h2>Managing debian/changelog</h2>

<p>This DEP makes no specific recommendation on how to manage the Debian
changelog. Some maintainers like to use tools like <code>gbp dch</code> to generate
the changelog based on Git commit notices, others edit the file manually
and use tools like <code>debcommit</code> to reuse the changelog entry in the
Git commit.</p>

<p>Helper tools should however configure the Git repository so that merges
of the <code>debian/changelog</code> file are handled by <code>dpkg-mergechangelogs</code> as
this will make it much easier to merge between different packaging
branches.</p>

<h2>When to not follow the above recommendations</h2>

<p>Most of the recommendations in this document assume that the upstream
developers use their Git repository in a traditional way: one software per
repository and creating tags for released versions.</p>

<p>When the upstream diverge from those conventions, you are entitled
to use your common sense and adapt those recommendations accordingly.
For example, if upstream tags contain the software name (e.g.
<code>foo-1.0</code> and <code>bar-2.0</code>), you should probably also consider embedding
the name in the tags used for your package releases.</p>

<h1>Changes</h1>

<ul>
<li>2014-11-05: Initial draft by Raphaël Hertzog.</li>
<li>2016-11-09: Extended version mangling to troublesome dots -- Ian Jackson.</li>
</ul>


</div>

<div id="footer">
<div id="pageinfo">







<div class="pagedate">
Last edited <span class="date">Thu, 10 Nov 2016 13:58:52 +0000</span>
</div>

</div>

<!-- from Debian Enhancement Proposals -->
</div>

</body>
</html>
